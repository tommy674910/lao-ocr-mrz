FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install pytesseract
RUN pip install tesseract
RUN pip install --no-cache-dir -r requirements.txt

# docker exec -it 85f107513708 bash
# RUN apt-get update && apt-get install -y && apt-get install tesseract-ocr -f -y && apt-get install libtesseract-dev -f -y && cp -r tessdata-master/. /usr/share/tesseract-ocr/4.00/tessdata/.


# rm -rf tessdata-master/configs
# COPY 
COPY . .

# RUN apt-get update && apt-get install -y
# RUN apt-get install tesseract-ocr -f -y
# RUN apt-get install libtesseract-dev -f -y
# RUN cp -r tessdata-master/. /usr/share/tesseract-ocr/4.00/tessdata/.

EXPOSE 8000

CMD [ "python", "./server.py" ]