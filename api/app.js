const http = require('http');

const axios = require('axios');
const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
    axios
    .post('http://10.2.100.95:8000/', {
        done: ''
    })
    .then(res => {
        console.log(`statusCode: ${res.statusCode}`)
        console.log(res)
    })
    .catch(error => {
        console.error(error)
    })
    res.statusCode = 200;
    res.setHeader('Content-Type', 'application/json');
    res.end('Hello World');
});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});