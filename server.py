from http.server import BaseHTTPRequestHandler, HTTPServer, SimpleHTTPRequestHandler, test
import sys
import cgi
from urllib.parse import non_hierarchical, urlparse
import base64
import json
from passporteye import read_mrz
import binascii


class CORSRequestHandler(SimpleHTTPRequestHandler):
    def end_headers (self):
        SimpleHTTPRequestHandler.end_headers(self)
    def do_OPTIONS(self):
        self.send_response(200, "ok")
        self.send_header('Content-type', 'application/json')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.send_header('Access-Control-Allow-Methods', '*')
        self.send_header("Access-Control-Allow-Headers", "*")
        self.end_headers()
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()
    def do_HEAD(self):
        self._set_headers()
    def do_GET(self):
        self._set_headers()
        self.wfile.write("<html><body><h1>Get Request Received!</h1></body></html>".encode("utf-8"))
    def do_POST(self):
        self.do_OPTIONS()
        length = int(self.headers['Content-Length'])
        if length == 0:
            message = json.dumps({'done': False,'mrz': False,'status': 'request is not be null'})
            self.wfile.write(message.encode())
            return
        ctype, pdict = cgi.parse_header(self.headers['content-type'])
        # pdict['boundary'] = bytes(pdict['boundary'], "utf-8")
        # # refuse to receive non-json content
        if ctype != 'application/json':
            return
            
        # read the message and convert it into a python dictionary
        
        message = json.loads(self.rfile.read(length))
        
        # add a property to the object, just to mess with data
        # Test write base64 result to file
        # lines = [message["done"]]
        # with open('readme.txt', 'w') as f:
        #     f.writelines(lines)

        # Write base64 to image
        if not 'done' in message or len(message['done']) == 0:
            message["done"] = False
            message["mrz"] = False
            message["status"] = 'request is not be null'
            self.wfile.write(json.dumps(message).encode())
            return
        if not message["done"] :
            message["done"] = False
            message["mrz"] = False
            message["status"] = 'request is not be null'
            self.wfile.write(json.dumps(message).encode())
            return
        if message["done"] != None:
            img_data = None
            try:
                img_data = str(message["done"])
            except TypeError:
                message["done"] = False
                message["mrz"] = False
                message["status"] = 'Is not image data'
                self.wfile.write(json.dumps(message).encode())
                return
            # print(img_data)
            # fh = open("imageToSave.png", "wb")
            # fh.write(base64.b64decode(img_data))
            # fh.close()
            # send the message back
            # parsed_path = urlparse(self.path)
            im_bytes = None
            try:
                im_bytes = base64.b64decode(img_data)
            except binascii.Error:
                message["done"] = False
                message["mrz"] = False
                message["status"] = 'binascii.Error'
                self.wfile.write(json.dumps(message).encode())
                return
            mrz = None
            try:
                mrz = read_mrz(im_bytes)
            except Exception as e:
                message["done"] = False
                message["mrz"] = False
                message["status"] = 'There was an exception '+str(e)
                self.wfile.write(json.dumps(message).encode())
                return

            if mrz is None:
                message["done"] = False
                message["mrz"] = False
                message["status"] = 'MRZ Can not read image'
                self.wfile.write(json.dumps(message).encode())
                return
            else:
                if mrz.to_dict().get('country') == 'LAO' :
                    mrz = read_mrz(im_bytes, extra_cmdline_params='--oem 0')
            mrzrawtxt = mrz.to_dict().get('raw_text')
            # print(rawtxt)
            # print(len(rawtxt))
            # print(rawtxt.find('LLLL'))
            # print(rawtxt.find('<LLLL'))
            # print(rawtxt.find('LLKL'))
            # print(rawtxt.find('LKKK'))
            # print(rawtxt.find('KKKK'))
            # print(rawtxt.find('KKKL'))
            # print(rawtxt.find('KKKK'))
            mrzrawtxt = mrzrawtxt.replace("<LLL", "<<<<")
            mrzrawtxt = mrzrawtxt.replace("<LLL", "<<<<")
            mrzrawtxt = mrzrawtxt.replace("<<L<", "<<<<")
            mrzrawtxt = mrzrawtxt.replace("LLKL", "<<<<")
            mrzrawtxt = mrzrawtxt.replace("LLLLLLLLLLL", "<<<<")
            mrzrawtxt = mrzrawtxt.replace("LLLLLLLLLL", "<<<<")
            mrzrawtxt = mrzrawtxt.replace("LLLLLLLLL", "<<<<")
            mrzrawtxt = mrzrawtxt.replace("LLLLLLLL", "<<<<")
            mrzrawtxt = mrzrawtxt.replace("LLLLLLL", "<<<<")
            mrzrawtxt = mrzrawtxt.replace("LLLLLL", "<<<<")
            mrzrawtxt = mrzrawtxt.replace("LLLLL", "<<<<")
            mrzrawtxt = mrzrawtxt.replace("LLLL", "<<<<")
            mrztype = mrz.to_dict().get('type')
            mrztype = mrztype.replace("<", "")
            mrzname = mrz.to_dict().get('names')
            mrzname = mrzname.replace("LLKL", "")
            mrzname = mrzname.replace("  LL", "")
            mrzname = mrzname.replace("   LL", "")
            mrzname = mrzname.replace("    LL", "")
            mrzname = mrzname.replace("   L", "")
            mrzname = mrzname.replace("    L", "")
            mrzname = mrzname.replace("LLLLLLLLLLLLLLL", "")
            mrzname = mrzname.replace("LLLLLLLLLLLLLL", "")
            mrzname = mrzname.replace("LLLLLLLLLLLLL", "")
            mrzname = mrzname.replace("LLLLLLLLLLLL", "")
            mrzname = mrzname.replace("LLLLLLLLLLL", "")
            mrzname = mrzname.replace("LLLLLLLLLL", "")
            mrzname = mrzname.replace("LLLLLLLLL", "")
            mrzname = mrzname.replace("LLLLLLLLL", "")
            mrzname = mrzname.replace("LLLLLLLL", "")
            mrzname = mrzname.replace("LLLLLLL", "")
            mrzname = mrzname.replace("LLLLLL", "")
            mrzname = mrzname.replace("LLLLL", "")
            mrzname = mrzname.replace("LLLLL", "")
            mrzname = mrzname.replace("LLLL", "")
            mrzname = mrzname.replace("  ", "")
            mrzname = mrzname.replace("   ", "")
            mrzsurname = mrz.to_dict().get('surname')
            mrzsurname = mrzsurname.replace("LLKL", "")
            mrzsurname = mrzsurname.replace("  LL", "")
            mrzsurname = mrzsurname.replace("   LL", "")
            mrzsurname = mrzsurname.replace("    LL", "")
            mrzsurname = mrzsurname.replace("   L", "")
            mrzsurname = mrzsurname.replace("    L", "")
            mrzsurname = mrzsurname.replace("LLLLLLLLLLLLLLL", "")
            mrzsurname = mrzsurname.replace("LLLLLLLLLLLLLL", "")
            mrzsurname = mrzsurname.replace("LLLLLLLLLLLLL", "")
            mrzsurname = mrzsurname.replace("LLLLLLLLLLLL", "")
            mrzsurname = mrzsurname.replace("LLLLLLLLLLL", "")
            mrzsurname = mrzsurname.replace("LLLLLLLLLL", "")
            mrzsurname = mrzsurname.replace("LLLLLLLLL", "")
            mrzsurname = mrzsurname.replace("LLLLLLLLL", "")
            mrzsurname = mrzsurname.replace("LLLLLLLL", "")
            mrzsurname = mrzsurname.replace("LLLLLLL", "")
            mrzsurname = mrzsurname.replace("LLLLLL", "")
            mrzsurname = mrzsurname.replace("LLLLL", "")
            mrzsurname = mrzsurname.replace("LLLLL", "")
            mrzsurname = mrzsurname.replace("LLLL", "")
            mrzsurname = mrzsurname.replace("  ", "")
            mrzsurname = mrzsurname.replace("   ", "")
            message["mrz"] = {
                'mrz_type': mrz.to_dict().get('mrz_type'),
                'valid_score': mrz.to_dict().get('valid_score'),
                'raw_text': mrzrawtxt,
                'type': mrztype,
                'country': mrz.to_dict().get('country'),
                'number': mrz.to_dict().get('number'),
                'date_of_birth': mrz.to_dict().get('date_of_birth'),
                'expiration_date': mrz.to_dict().get('expiration_date'),
                'nationality': mrz.to_dict().get('nationality'),
                'sex': mrz.to_dict().get('sex'),
                'names': mrzname,
                'surname': mrzsurname,
                'personal_number': mrz.to_dict().get('personal_number'),
                'check_number': mrz.to_dict().get('check_number'),
                'check_date_of_birth': mrz.to_dict().get('check_date_of_birth'),
                'check_expiration_date': mrz.to_dict().get('check_expiration_date'),
                'check_composite': mrz.to_dict().get('check_composite'),
                'check_personal_number': mrz.to_dict().get('check_personal_number'),
                'valid_number': mrz.to_dict().get('valid_number'),
                'valid_date_of_birth': mrz.to_dict().get('valid_date_of_birth'),
                'valid_expiration_date': mrz.to_dict().get('valid_expiration_date'),
                'valid_composite': mrz.to_dict().get('valid_composite'),
                'valid_personal_number': mrz.to_dict().get('valid_personal_number'),
                'method': mrz.to_dict().get('method'),
            }
            message["done"] = True
            message["status"] = 'success'
            self.wfile.write(json.dumps(message).encode())

def run(server_class=HTTPServer, handler_class=CORSRequestHandler, port=8000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print ('Server running at localhost:8000')
    httpd.serve_forever()

run()